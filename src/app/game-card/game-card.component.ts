import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { GameDetails } from '../Models/game-details'

@Component({
  selector: 'app-game-card',
  templateUrl: './game-card.component.html',
  styleUrls: ['./game-card.component.scss']
})
export class GameCardComponent implements OnInit {

  constructor(private router: Router) { }

  imgSrc: string;
  goalAchievement: number;
  @Input() gameInfo: GameDetails;

  ngOnInit(): void {
    this.imgSrc = environment.baseUrl + '/' + this.gameInfo.feature_avatar.hdpi;
    this.goalAchievement = this.gameInfo.invest_goal.achievement_percent;
  }

  detailsGame(id: number) {
    this.router.navigate(['projects/details', id])
  }

}
