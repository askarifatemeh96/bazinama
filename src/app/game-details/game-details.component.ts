import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { ProductService } from '../Services/product.service';
import { GameDetails } from '../Models/game-details'
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-game-details',
  templateUrl: './game-details.component.html',
  styleUrls: ['./game-details.component.scss']
})
export class GameDetailsComponent implements OnInit {

  constructor(private activateRoute: ActivatedRoute,
    private productService: ProductService) { }

  gameId: number;
  gameDetails: GameDetails;
  imgSrc: string;
  goalAchievement: number;

  ngOnInit(): void {
    this.getGameId();
    this.getGameDetails();
  }

  getGameId() {
    this.gameId = this.activateRoute.snapshot.params.id;
  }

  getGameDetails() {
    this.productService.getProductDetails(this.gameId)
      .subscribe(data => {
        this.gameDetails = data;
        this.imgSrc = environment.baseUrl + '/' + data.feature_avatar.hdpi;
        this.goalAchievement = data.invest_goal.achievement_percent;
      })
  };

}
