import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  postLoginStep1(userInfo1: any) {
    return this.http.post<any>(`${environment.baseUrl}/mobile_login_step1/5`, userInfo1)
  }

  postLoginStep2(userInfo2: any) {
    return this.http.post<any>(`${environment.baseUrl}/mobile_login_step2/5`, userInfo2)
  }
}
