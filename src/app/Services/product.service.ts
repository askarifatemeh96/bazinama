import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  public baseUrl: string = environment.baseUrl;
  getProductDetailURL = 'https://api.vasapi.click/product/11181?device_os=ios';

  getProductList(): Observable<any> {
    return this.http.get(`${this.baseUrl}/listproducts/233?limit=6&offset=0`)
  }

  getProductDetails(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/product/${id}?device_os=ios`)
  }
}

