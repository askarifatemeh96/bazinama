import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UserService } from '../Services/user.service';

@Component({
  selector: 'app-dialog-login',
  templateUrl: './dialog-login.component.html',
  styleUrls: ['./dialog-login.component.scss']
})
export class DialogLoginComponent implements OnInit {

  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

  loginPage: boolean = true;
  verificationCodePage: boolean = false;
  userInfoStep1: any;
  userInfoStep2: any;
  loginInfoStep1 = new FormGroup({
    mobile: new FormControl(''),
    device_os: new FormControl('angularJS'),
    device_id: new FormControl('browser'),
    device_model: new FormControl('web')
  });

  loginInfoStep2 = new FormGroup({
    verification_code: new FormControl(''),
    device_os: new FormControl('angularJS'),
    device_id: new FormControl('browser'),
    device_model: new FormControl('web'),
    mobile: new FormControl(''),
    nickname: new FormControl(''),
  });

  onSubmitStep1() {
    if (!this.loginInfoStep1.valid) return;
    this.userInfoStep1 = this.loginInfoStep1.value;
    this.postLoginStep1(this.userInfoStep1);
  }

  onSubmitStep2() {
    if (!this.loginInfoStep2.valid) return;
    this.userInfoStep2 = this.loginInfoStep2.value;
    this.postLoginStep2(this.userInfoStep2);

  }

  postLoginStep1(userInfoStep1) {
    this.userService.postLoginStep1(userInfoStep1)
      .subscribe((data) => {
        this.loginPage = false;
        this.verificationCodePage = true;
        this.loginInfoStep2.patchValue({
          nickname: data.nickname,
          mobile: this.loginInfoStep1.get('mobile').value
        });
      })
  }

  postLoginStep2(userInfoStep2) {
    this.userService.postLoginStep2(userInfoStep2)
      .subscribe((data) => {
        localStorage.setItem('token', JSON.stringify(data));
        location.reload();
      })
  }
}
