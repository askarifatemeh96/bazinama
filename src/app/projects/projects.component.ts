import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../app/Services/product.service';
import { GameList } from '../Models/game-list'

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  constructor(private productService: ProductService) { }

  listOfGame: GameList;

  ngOnInit(): void {
    this.getProductList();
  }

  getProductList() {
    this.productService.getProductList()
      .subscribe((data) => { this.listOfGame = data; })
  }

}
