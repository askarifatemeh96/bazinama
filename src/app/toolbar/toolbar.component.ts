import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogLoginComponent } from '../dialog-login/dialog-login.component';
import { AuthService } from 'src/app/Services/auth.service'

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  constructor(public dialog: MatDialog,
    private authService: AuthService) { }

  nickname: any;
  ngOnInit(): void {
    this.isLoggedIn();
  }

  openDialog() {
    this.dialog.open(DialogLoginComponent, {
      panelClass: "custom-dialog"
    });
  }

  isLoggedIn(): boolean {
    if (this.authService.isLoggedIn())
      this.getNickname();
    return this.authService.isLoggedIn();
  };

  loggedOut() {
    return this.authService.loggedOut();
  }

  getNickname() {
    this.nickname = JSON.parse(this.authService.getNickname());
  }

}
