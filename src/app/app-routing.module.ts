import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuestionsComponent } from './questions/questions.component';
import { SupportComponent } from './support/support.component';
import { MainContentComponent } from './main-content/main-content.component';
import { ProjectsComponent } from './projects/projects.component';
import { GameDetailsComponent } from './game-details/game-details.component';

const routes: Routes = [
  { path: '', component: MainContentComponent },
  { path: 'projects', component: ProjectsComponent },
  { path: 'projects/details/:id', component: GameDetailsComponent },
  { path: 'questions', component: QuestionsComponent },
  { path: 'support', component: SupportComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
